cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-device.device",
        "file": "plugins/cordova-plugin-device/www/device.js",
        "pluginId": "cordova-plugin-device",
        "clobbers": [
            "device"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.0",
    "cordova-plugin-crosswalk-webview": "2.2.0",
    "cordova-opentok-android-permissions": "1.0.0",
    "cordova-plugin-iosrtc": "3.2.0",
    "cordova-plugin-device": "1.1.4"
};
// BOTTOM OF METADATA
});